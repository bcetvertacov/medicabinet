﻿using Contract.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BE.ConfigurationContext
{
    public class ConfigureBookingForms : EntityTypeConfiguration<BookingForm>
    {
        public ConfigureBookingForms()
        {
            this.Property(x => x.NameSurname).IsRequired();
            this.Property(x => x.BookingDate).IsRequired();
            this.Property(x => x.Phone).IsRequired();
            this.Property(x => x.Email).IsRequired();
            this.Property(x => x.CreatedOn).IsRequired();

            this.HasRequired(x => x.User)
                .WithMany(x => x.BookingForms)
                .HasForeignKey(x => x.UserId);                

            this.HasRequired(x => x.Procedure)
                .WithMany(x => x.BookingForms)
                .HasForeignKey(x => x.ProcedureId);
        }
    }
}
