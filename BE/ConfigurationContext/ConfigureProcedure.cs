﻿using Contract.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BE.ConfigurationContext
{
    public class ConfigureProcedure : EntityTypeConfiguration<Procedure>
    {
        public ConfigureProcedure()
        {
            this.Property(x => x.Description).IsRequired();
            this.Property(x => x.Type).IsRequired();
        }
    }
}
