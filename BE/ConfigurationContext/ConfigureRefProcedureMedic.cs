﻿using Contract.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BE.ConfigurationContext
{
    public class ConfigureRefProcedureMedic : EntityTypeConfiguration<RefProcedureMedic>
    {
        public ConfigureRefProcedureMedic()
        {
            this.HasKey(
                x => new { x.ProcedureId, x.UserId }            
            );

            this.HasRequired(x => x.User)
                .WithMany(x => x.Procedures)
                .HasForeignKey(x => x.ProcedureId);

            this.HasRequired(x => x.Procedure)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.UserId);
        }
    }
}
