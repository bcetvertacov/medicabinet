﻿using Contract.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BE.ConfigurationContext
{
    public class ConfigureUser : EntityTypeConfiguration<User>
    {
        public ConfigureUser()
        {
            this.Property(x => x.Password).IsRequired();
            this.Property(x => x.Schedule).IsRequired();

            this.HasRequired(x => x.PersonalDetails)
                .WithOptional(x => x.User);
        }
    }
}
