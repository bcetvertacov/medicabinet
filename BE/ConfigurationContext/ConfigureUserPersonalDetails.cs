﻿using Contract.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BE.ConfigurationContext
{
    public class ConfigureUserPersonalDetails : EntityTypeConfiguration<UserPersonalDetails>
    {
        public ConfigureUserPersonalDetails()
        {
            this.Property(x => x.Name).IsRequired();
            this.Property(x => x.Surname).IsRequired();
            this.Property(x => x.Phone).IsRequired();
            this.Property(x => x.Email).IsRequired();
        }
    }
}
