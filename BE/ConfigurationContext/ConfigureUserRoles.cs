﻿using Contract.Entities;
using System;
using System.Data.Entity.ModelConfiguration;

namespace BE.ConfigurationContext
{
    public class ConfigureUserRoles : EntityTypeConfiguration<UserRole>
    {
        public ConfigureUserRoles()
        {
            this.Property(x => x.Description).IsRequired();
            this.Property(x => x.Type).IsRequired();
            this.Property(x =>x.Type).IsRequired();
        }
    }
}
