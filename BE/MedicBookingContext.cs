﻿using BE.ConfigurationContext;
using Contract.Entities;
using System.Data.Entity;

namespace BE
{
    public class MedicBookingContext : DbContext
    {
        public DbSet<BookingForm> BookingForms { get; set; }
        public DbSet<Procedure> Procedures { get; set; }
        public DbSet<RefProcedureMedic> RefProcedureMedics { get; set; }
        public DbSet<UserPersonalDetails> UserPersonalDetails { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<User> Users { get; set; }

        public MedicBookingContext() : base("MedicCabinetDb")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<MedicBookingContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ConfigureBookingForms());
            modelBuilder.Configurations.Add(new ConfigureRefProcedureMedic());
            modelBuilder.Configurations.Add(new ConfigureProcedure());
            modelBuilder.Configurations.Add(new ConfigureUser());
            modelBuilder.Configurations.Add(new ConfigureUserPersonalDetails());
            modelBuilder.Configurations.Add(new ConfigureUserRoles());
        }
    }
}
