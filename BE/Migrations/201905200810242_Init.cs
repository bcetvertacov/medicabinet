namespace BE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookingForms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BookingDate = c.DateTime(nullable: false),
                        NameSurname = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Comments = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        UserId = c.Guid(nullable: false),
                        ProcedureId = c.Guid(nullable: false),
                        FormStatusId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FormStatus", t => t.FormStatusId, cascadeDelete: true)
                .ForeignKey("dbo.Procedures", t => t.ProcedureId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ProcedureId)
                .Index(t => t.FormStatusId);
            
            CreateTable(
                "dbo.FormStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Procedures",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RefProcedureMedics",
                c => new
                    {
                        ProcedureId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProcedureId, t.UserId })
                .ForeignKey("dbo.Procedures", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.ProcedureId, cascadeDelete: true)
                .Index(t => t.ProcedureId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Password = c.String(nullable: false),
                        Schedule = c.String(nullable: false),
                        PersonalDetailsId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserPersonalDetails", t => t.Id)
                .ForeignKey("dbo.UserRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserPersonalDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.String(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookingForms", "UserId", "dbo.Users");
            DropForeignKey("dbo.BookingForms", "ProcedureId", "dbo.Procedures");
            DropForeignKey("dbo.RefProcedureMedics", "ProcedureId", "dbo.Users");
            DropForeignKey("dbo.Users", "RoleId", "dbo.UserRoles");
            DropForeignKey("dbo.Users", "Id", "dbo.UserPersonalDetails");
            DropForeignKey("dbo.RefProcedureMedics", "UserId", "dbo.Procedures");
            DropForeignKey("dbo.BookingForms", "FormStatusId", "dbo.FormStatus");
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.Users", new[] { "Id" });
            DropIndex("dbo.RefProcedureMedics", new[] { "UserId" });
            DropIndex("dbo.RefProcedureMedics", new[] { "ProcedureId" });
            DropIndex("dbo.BookingForms", new[] { "FormStatusId" });
            DropIndex("dbo.BookingForms", new[] { "ProcedureId" });
            DropIndex("dbo.BookingForms", new[] { "UserId" });
            DropTable("dbo.UserRoles");
            DropTable("dbo.UserPersonalDetails");
            DropTable("dbo.Users");
            DropTable("dbo.RefProcedureMedics");
            DropTable("dbo.Procedures");
            DropTable("dbo.FormStatus");
            DropTable("dbo.BookingForms");
        }
    }
}
