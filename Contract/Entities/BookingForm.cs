﻿using System;

namespace Contract.Entities
{
    public class BookingForm
    {
        public Guid Id { get; set; }
        public DateTime BookingDate { get; set; }
        public string NameSurname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime Updated { get; set; }      

        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid ProcedureId { get; set; }
        public Procedure Procedure { get; set; }

        public Guid FormStatusId { get; set; }
        public FormStatus FormStatus { get; set; }
    }
}
