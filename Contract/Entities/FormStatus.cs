﻿using System;
using System.Collections.Generic;

namespace Contract.Entities
{
    public class FormStatus
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<BookingForm> BookingForms { get; set; }
    }
}
