﻿using System;
using System.Collections.Generic;

namespace Contract.Entities
{
    public class Procedure
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public ICollection<BookingForm> BookingForms { get; set; }
        public ICollection<RefProcedureMedic> Users { get; set; }
    }
}
