﻿using System;

namespace Contract.Entities
{
    public class RefProcedureMedic
    {     
        public Guid UserId { get; set; }
        public Guid ProcedureId { get; set; }

        public Procedure Procedure { get; set; }
        public User User { get; set; }
    }
}
