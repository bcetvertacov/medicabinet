﻿using System;
using System.Collections.Generic;

namespace Contract.Entities
{
    public class User
    {
        public Guid Id { get; set; }
        public string Password { get; set; }
        public string Schedule { get; set; }

        public Guid PersonalDetailsId { get; set; }
        public UserPersonalDetails PersonalDetails { get; set; }

        public Guid RoleId { get; set; }
        public UserRole Role { get; set; }

        public ICollection<BookingForm> BookingForms { get; set; }
        public ICollection<RefProcedureMedic> Procedures { get; set; }
    }
}
