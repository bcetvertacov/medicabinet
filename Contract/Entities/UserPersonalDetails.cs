﻿using System;

namespace Contract.Entities
{
    public class UserPersonalDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
       
        public User User { get; set; }
    }
}
