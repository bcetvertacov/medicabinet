﻿using System;
using System.Collections.Generic;

namespace Contract.Entities
{
    public class UserRole
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public ICollection<User>Users { get; set; }
    }
}
